import express from 'express'
import path from 'path'
import 'express-async-errors'
import './database/connection'
import routes from './routes'
import handler from './errors/handler'
import cors from 'cors'

const app = express()

app.use(cors())
app.use(express.json())
app.use(routes)
app.use('/uploads', express.static(path.join(__dirname, '..', 'uploads')))
app.use(handler)
app.listen(3333)

console.log('Executando em localhost 3333')