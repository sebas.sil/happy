import Orphanage from '../database/models/Orphanage'
import ImageView from '../views/ImagesView'

const imageView = new ImageView()

class OrphanageView {

    public render(orphanage: Orphanage) : any;
    public render(orphanages: Orphanage[]) : any;

    public render(o: any) : any{
        if(Array.isArray(o)){
            return o.map(orpha => this.convert(orpha))
        } else {
            return this.convert(o)
        }
    }

    private convert(orphanage: Orphanage) : any {
        return {
            id: orphanage.id,
            name: orphanage.name,
            latitude: orphanage.latitude,
            longitude: orphanage.longitude,
            about: orphanage.about,
            instructions: orphanage.instructions,
            opening_hours: orphanage.opening_hours,
            open_on_weekends: orphanage.open_on_weekends,
            images: imageView.render(orphanage.images)
        }
    }
}

export default OrphanageView