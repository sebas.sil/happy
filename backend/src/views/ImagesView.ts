import Image from '../database/models/Image'
class ImageView {

    public render(image: Image) : any
    public render(images: Image[]) : any

    public render(i: any) : any{
        if(Array.isArray(i)){
            return i.map(img => this.convert(img))
        } else {
            return this.convert(i)
        }
    }

    private convert(image: Image) : any {
        return {
            id: image.id,
            url: `http://localhost:3333/uploads/${image.path}`
        }
    }
}

export default ImageView