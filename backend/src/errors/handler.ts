import { ErrorRequestHandler } from 'express'
import { ValidationError } from 'yup'

interface InternalValidationError {
    [key: string]:string[]
}
const handler:ErrorRequestHandler = (error, request, response, next) => {
    console.log(error)
    let mensagem = 'erro ao processar a request'
    let errors:InternalValidationError = {}
    if(error instanceof ValidationError){
        mensagem = 'erro na validação dos dados'
        error.inner.forEach(e => {
            if(e.path){
                errors[e.path] = e.errors
            }
        })
    }
    return response.status(400).json({mensagem, errors})
}

export default handler