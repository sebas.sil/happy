import multer from 'multer'
import path from 'path'

const config = {
    storage: multer.diskStorage({
        destination: path.join(__dirname, '..', '..', 'uploads'),
        filename: (request, file, cb) => {
            const timestamp = process.hrtime()
            // seconds + remains seconds in nanoseconds + extension
            const filename = '' + timestamp[0] + timestamp[1] + path.extname(file.originalname)
            cb(null, filename)
        }
    })
}

export default config