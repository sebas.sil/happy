import { Router } from 'express'
import OrphanagesControl from './controllers/OrphanagesController'
import multer from 'multer'
import uploadConfig from '../src/config/multer'

const routes = Router()
const upload = multer(uploadConfig)

routes.post('/orphanages', upload.array('images'),OrphanagesControl.create)
routes.get('/orphanages', OrphanagesControl.index)
routes.get('/orphanages/:id', OrphanagesControl.show)

export default routes