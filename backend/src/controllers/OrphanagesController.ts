import {Request, Response} from 'express'
import { getRepository } from 'typeorm'
import Orphanage from '../database/models/Orphanage'
import OrphanageView from '../views/OrphanagesView'
import * as Yup from 'yup'

const OrphanagesControl = {
    async create(request: Request, response: Response) {
        const orphanageRep = getRepository(Orphanage)
        
        const files = request.files as Express.Multer.File[]

        const images = files.map(i => ({path: i.filename}))

        const schema = Yup.object().shape({
            name: Yup.string().required(),
            latitude: Yup.number().required(),
            longitude: Yup.number().required(),
            about: Yup.string().required().max(300),
            instructions: Yup.string().required().max(300),
            opening_hours: Yup.string().required(),
            open_on_weekends: Yup.boolean().required(),
            images: Yup.array(Yup.object().shape({path:Yup.string().required()})).required()
        })

        const data = {...request.body, images}
        
        const finalData = await schema.validate(data, {
            abortEarly: false
        })
        
        const orphanages = orphanageRep.create(finalData)

        await orphanageRep.save(orphanages)

        return response.status(201).json(new OrphanageView().render(orphanages))
    },

    async index(request: Request, response: Response) {
        const orphanageRep = getRepository(Orphanage)
        const orphanages = await orphanageRep.find({ relations: ["images"] })

        return response.json(new OrphanageView().render(orphanages))
    },

    async show(request: Request, response: Response) {
        const { id } = request.params;
        const orphanageRep = getRepository(Orphanage)
        const orphanage = await orphanageRep.findOneOrFail(id, { relations: ["images"] })

        return response.json(new OrphanageView().render(orphanage))
    }
}

export default OrphanagesControl