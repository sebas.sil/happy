import React, {useEffect, useState} from "react"
import {FaWhatsapp} from "react-icons/fa"
import {FiClock, FiInfo} from "react-icons/fi"
import {MapContainer, Marker, TileLayer} from "react-leaflet"
import Sidebar from '../components/Sidebar'
import api from "../services/api"
import {useParams} from 'react-router-dom'

import '../styles/orphanage.css'

import happyMapIcon from '../utils/mapIcon'
import OrphanageInterface from "../utils/OrphanageInterface"

interface OrphanageParams {
  id: string
}

export default function Orphanage() {
  const params = useParams<OrphanageParams>()

  const [orphanage, setOrphanage] = useState<OrphanageInterface>()
  const [activeImgIdx, setActiveImgIdx] = useState<number>(0)

  useEffect(() => {
    api.get(`orphanages/${params.id}`).then(res => {
      console.log(res.data)
      setOrphanage(res.data)
    })
  }, [params.id])

  if (!orphanage) {
    return <p>Carregando...</p>
  }
  
  return (
    <div id="page-orphanage">

      <Sidebar />

      <main>
        <div className="orphanage-details">
        {
              orphanage.images.length > 0 && <img src={orphanage.images[activeImgIdx].url} alt="preview" />
        }
          

          <div className="images">
            {
              orphanage.images.map((img, idx) => (
                <button key={img.id} className={idx === activeImgIdx ? "active" : ''} type="button" onClick={() => setActiveImgIdx(idx)}>
                  <img src={img.url} alt='preview' />
                </button>
              ))
            }
          </div>

          <div className="orphanage-details-content">
            <h1>{orphanage.name}</h1>
            <p>{orphanage.about}</p>

            <div className="map-container">
              <MapContainer
                center={[orphanage.latitude, orphanage.longitude]}
                zoom={16}
                style={{width: '100%', height: 280}}
                dragging={false}
                touchZoom={false}
                zoomControl={false}
                scrollWheelZoom={false}
                doubleClickZoom={false}
              >
                <TileLayer
                  url='https://a.tile.openstreetmap.org/{z}/{x}/{y}.png'
                />
                <Marker interactive={false} icon={happyMapIcon} position={[orphanage.latitude, orphanage.longitude]} />
              </MapContainer>

              <footer>
                <a target="_blank" rel="noopener noreferrer" href={`https://www.google.com/maps/dir/?api=1&destination=${orphanage.latitude},${orphanage.longitude}`}>Ver rotas no Google Maps</a>
              </footer>
            </div>

            <hr />

            <h2>Instruções para visita</h2>
            <p>{orphanage.instructions}</p>

            <div className="open-details">
              <div className="hour">
                <FiClock size={32} color="#15B6D6" />
                {orphanage.opening_hours}
              </div>
              <div className={orphanage.open_on_weekends ? 'open-on-weekends' : 'dont-open-on-weekends'}>
                <FiInfo size={32} color={orphanage.open_on_weekends ? "#39CC83" : "#FF669D"}/>
                {orphanage.open_on_weekends ? 'A' : 'Não a'}
                tendemos <br /> fim de semana
              </div>
            </div>

            <button type="button" className="contact-button">
              <FaWhatsapp size={20} color="#FFF" />
              Entrar em contato
            </button>
          </div>
        </div>
      </main>
    </div>
  )
}