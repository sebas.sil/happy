import React, {ChangeEvent, FormEvent, useState} from "react"
import { MapContainer, Marker, TileLayer, useMapEvents } from 'react-leaflet'

import { FiPlus } from "react-icons/fi"

import Sidebar from'../components/Sidebar'

import happyMapIcon from '../utils/mapIcon'

import api from '../services/api'


import '../styles/create-orphanage.css'
import {useHistory} from "react-router-dom"

export default function CreateOrphanage() {
  const [position, setPosition] = useState<number[]>([0,0])
  const [name, setName] = useState<string>('')
  const [about, setAbout] = useState<string>('')
  const [instructions, setInstructions] = useState<string>('')
  const [openingOn, setOpeningOn] = useState<string>('')
  const [openWeekends, setOpenWeekends] = useState<boolean>(false)
  const [images, setImages] = useState<File[]>([])
  const [imagesPreview, setImagesPreview] = useState<string[]>([])
  const history = useHistory()

  interface LocationMarquerParms {
    onClick(position: number[]):void;
  }
  const LocationMarker: React.FC<LocationMarquerParms> = ({ onClick }) => {
    
    useMapEvents({
      click(e) {
        onClick([e.latlng.lat, e.latlng.lng])
      }
    })
  
    return position === null ? null : (
      <Marker icon={happyMapIcon} position={[position[0],position[1]]} />
    )
  }

  async function handleSubmit(evt:FormEvent){
    evt.preventDefault()

    const data = new FormData()
    data.append('name', name)
    data.append('latitude', String(position[0]))
    data.append('longitude', String(position[1]))
    data.append('about', about)
    data.append('instructions', instructions)
    data.append('opening_hours', openingOn)
    data.append('open_on_weekends', String(openWeekends))

    images.forEach(i => {
      data.append('images', i)
    })

    await api.post('orphanages', data).then(res => {
      console.log(res)
    })

    history.goBack()
  }

  function handleSeletedImagens(evt:ChangeEvent<HTMLInputElement>){
    if(evt.target.files){
      const selected = Array.from(evt.target.files)
      setImages(selected)
      const previews = selected.map(e => {
        return URL.createObjectURL(e);
      })

      setImagesPreview(previews)
    }
  }
  
  return (
    <div id="page-create-orphanage">
      
      <Sidebar />

      <main>
        <form className="create-orphanage-form" onSubmit={handleSubmit}>
          <fieldset>
            <legend>Dados</legend>

            <MapContainer 
              center={[-27.2092052,-49.6401092]} 
              style={{ width: '100%', height: 280 }}
              zoom={15}
            >
              <TileLayer 
                url='https://a.tile.openstreetmap.org/{z}/{x}/{y}.png'
              />

              <LocationMarker onClick={setPosition}/>
            </MapContainer>

            <div className="input-block">
              <label htmlFor="name">Nome</label>
              <input id="name" onChange={e => setName(e.target.value)}/>
            </div>

            <div className="input-block">
              <label htmlFor="about">Sobre <span>Máximo de 300 caracteres</span></label>
              <textarea id="name" maxLength={300} onChange={e => setAbout(e.target.value)}/>
            </div>

            <div className="input-block">
              <label htmlFor="images">Fotos</label>

              <div className="images-container">
                {
                  imagesPreview.map((e, i) => (
                    <img key={i} src={e} alt="preview" />
                  ))
                }
                <label htmlFor='images' className="new-image">
                  <FiPlus size={24} color="#15b6d6" />
                </label>
              </div>

              <input multiple onChange={handleSeletedImagens} type='file' id='images' />
            </div>
          </fieldset>

          <fieldset>
            <legend>Visitação</legend>

            <div className="input-block">
              <label htmlFor="instructions">Instruções</label>
              <textarea id="instructions" onChange={e => setInstructions(e.target.value)}/>
            </div>

            <div className="input-block">
              <label htmlFor="opening_hours">Horario de funcionamento</label>
              <input id="opening_hours" onChange={e => setOpeningOn(e.target.value)}/>
            </div>

            <div className="input-block">
              <label htmlFor="open_on_weekends">Atende fim de semana</label>

              <div className="button-select">
                <button type="button" className={openWeekends ? "active" : ""} onClick={e => setOpenWeekends(true)}>Sim</button>
                <button type="button" className={openWeekends ? "" : "active"} onClick={e => setOpenWeekends(false)}>Não</button>
              </div>
            </div>
          </fieldset>

          <button className="confirm-button" type="submit">
            Confirmar
          </button>
        </form>
      </main>
    </div>
  )
}

// return `https://a.tile.openstreetmap.org/${z}/${x}/${y}.png`;
