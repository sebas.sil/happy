import React, {useEffect, useState} from 'react'
import mapMarker from '../images/map-marker.svg'
import {Link} from 'react-router-dom'
import {FiPlus, FiArrowRight} from 'react-icons/fi'
import {MapContainer, TileLayer, Marker, Popup} from 'react-leaflet'
import happyMapIcon from '../utils/mapIcon'
import api from '../services/api'
import OrphanageInterface from '../utils/OrphanageInterface'

import '../styles/orphanagesMap.css'
import 'leaflet/dist/leaflet.css'

const OrfanagesMap = () => {

    const [orphanages, setOrphanages] = useState<OrphanageInterface[]>([])

    useEffect(() => {
        api.get('orphanages').then(res => {
            setOrphanages(res.data)
        })
    }, [])

    return (
        <div id="page-map">
            <aside>
                <header>
                    <img src={mapMarker} alt="marker" />
                    <h2>Escolha um orfanato no mapa</h2>
                    <p>Muitas crianças estão esperando a usa visita ;)</p>
                </header>

                <footer>
                    <strong>Curitiba</strong>
                    <span>Paraná</span>
                </footer>
            </aside>

            <MapContainer center={[-25.3895263, -49.2312056]} zoom={13} style={{width: '100%', height: '100%'}}>
                <TileLayer url='https://a.tile.openstreetmap.org/{z}/{x}/{y}.png' />

                {
                    orphanages.map(orphanage => (
                        <Marker key={orphanage.id} position={[orphanage.latitude, orphanage.longitude]} icon={happyMapIcon}>
                            <Popup closeButton={false} minWidth={240} maxWidth={240}>
                                {orphanage.name}
                                <Link to={`/orphanages/${orphanage.id}`}>
                                    <FiArrowRight size={20} color='#FFF' />
                                </Link>
                            </Popup>
                        </Marker>
                    ))
                }
            </MapContainer>

            <Link to="/orphanages/create" className='create-orphanage'>
                <FiPlus size={32} color='#FFF' />
            </Link>
        </div>
    )
}

export default OrfanagesMap